INSERT INTO BRAND(ID, NAME) VALUES (1, 'ZARA');

INSERT INTO PRODUCT(ID, NAME) VALUES (35455, 'Product #1');

INSERT INTO PRICE_LIST(ID, NAME) VALUES (1, 'Price List #1');
INSERT INTO PRICE_LIST(ID, NAME) VALUES (2, 'Price List #2');
INSERT INTO PRICE_LIST(ID, NAME) VALUES (3, 'Price List #3');
INSERT INTO PRICE_LIST(ID, NAME) VALUES (4, 'Price List #4');

INSERT INTO PRICE(ID,BRAND_ID, START_DATE, END_DATE, PRICE_LIST_ID, PRODUCT_ID, PRIORITY, PRICE, CURR) VALUES 
(1, 1, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 1, 35455, 0, 35.50, 0),
(2, 1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455, 1, 25.45, 0),
(3, 1, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455, 1, 30.50, 0),
(4, 1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455, 1, 38.95, 0);