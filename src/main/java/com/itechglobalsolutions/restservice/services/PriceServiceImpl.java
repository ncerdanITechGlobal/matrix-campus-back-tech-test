package com.itechglobalsolutions.restservice.services;

import static java.util.Calendar.getInstance;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.itechglobalsolutions.restservice.model.Price;
import com.itechglobalsolutions.restservice.repository.PriceRepository;

@Service
public class PriceServiceImpl implements PriceService{

  @Autowired
  private PriceRepository priceRepo;

  @Override
  public List<Price> getPrices() {
    return priceRepo.findAll();
  }

  @Override
  public Page<Price> getPagedPrices() {
    return priceRepo.findAll(PageRequest.of(0,10));
  }

  @Override
  public Price getPriceByBrandProductAndBetweenDate(String brandId, String productId, Long appDateTimestamp) {
    try {
      Date appDate = parseTimestamp(appDateTimestamp);

      List<Price> list = priceRepo.findByBrandProductAndDate(brandId, productId, appDate);
      return list.get(0);
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return null;
    }
    
  }

  /**
   * Creates a new date from the millis received.
   * @param appDateTimestamp
   * @return
   */
  private Date parseTimestamp(Long appDateTimestamp) {
    Calendar cal = getInstance();
    cal.setTimeInMillis(appDateTimestamp);
    cal.setTimeZone(TimeZone.getTimeZone("UTC"));
    return cal.getTime();
  }

}
