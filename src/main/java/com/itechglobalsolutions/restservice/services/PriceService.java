package com.itechglobalsolutions.restservice.services;

import java.util.List;

import org.springframework.data.domain.Page;

import com.itechglobalsolutions.restservice.model.Price;

public interface PriceService {
  public List<Price> getPrices();

  public Page<Price> getPagedPrices();

  /**
   * Gets the highest priority Price that fulfills the brand and product id conditions. 
   * Furthermore, the given date must be between start and end dates.
   * @param brandId Brand Identifier.
   * @param productId Product Identifier
   * @param appDateTimestamp Date where the price is active.
   * @return highest priority price
   */
  public Price getPriceByBrandProductAndBetweenDate(String brandId, String productId, Long appDateTimestamp);
}
