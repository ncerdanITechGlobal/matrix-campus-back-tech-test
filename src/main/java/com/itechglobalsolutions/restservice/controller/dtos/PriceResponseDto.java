package com.itechglobalsolutions.restservice.controller.dtos;

import java.util.Date;

public record PriceResponseDto(int productId,int brandId, Date startDate,Date endDate, double price, int priceList){};
