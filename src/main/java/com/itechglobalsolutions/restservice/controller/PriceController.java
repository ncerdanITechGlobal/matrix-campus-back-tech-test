package com.itechglobalsolutions.restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itechglobalsolutions.restservice.controller.dtos.PriceResponseDto;
import com.itechglobalsolutions.restservice.model.Price;
import com.itechglobalsolutions.restservice.services.PriceService;

@RestController
public class PriceController {

  @Autowired
  PriceService priceService;

  @GetMapping("/prices/{brandId}/{productId}")
  public PriceResponseDto getPriceToApply(
    @PathVariable("brandId") String brandId,
    @PathVariable("productId") String productId,
    @RequestParam("date") Long date
  ){
    
    Price price = priceService.getPriceByBrandProductAndBetweenDate(brandId, productId, date);
    if (price == null) return null;
    return new PriceResponseDto(
      price.getProduct().getId(),
      price.getBrand().getId(),
      price.getStartDate(),
      price.getEndDate(),
      price.getPrice(),
      price.getPriceList().getId()
    );
  }

}