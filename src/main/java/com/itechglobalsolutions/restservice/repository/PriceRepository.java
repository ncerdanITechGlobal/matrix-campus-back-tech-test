package com.itechglobalsolutions.restservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.itechglobalsolutions.restservice.model.Price;

@Repository
public interface PriceRepository extends PagingAndSortingRepository<Price, Long> {
  List<Price> findAll();

  @Query(
    "SELECT p FROM Price p "+
    "WHERE p.brand.id = :brandId "+
      "and p.product.id = :productId "+
      "and :applicationDate BETWEEN p.startDate and p.endDate "+
    "ORDER BY p.priority DESC"
  )
  public List<Price> findByBrandProductAndDate(
    @Param("brandId") String brandId, 
    @Param("productId") String productId,
    @Param("applicationDate") Date applicationDate
  );
}
