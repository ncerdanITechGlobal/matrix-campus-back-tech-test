package com.itechglobalsolutions.restservice.model;

import java.util.Date;

import jakarta.persistence.Basic;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
@SequenceGenerator(name = "price_seq", initialValue = 1)
public class Price {
  
    @Id
    @GeneratedValue(generator = "price_seq")
    private Long id;
  
    @ManyToOne
    private Brand brand;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    
    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @ManyToOne
    private PriceList priceList;
    
    @ManyToOne
    private Product product;
    
    private int priority;
    
    private double price;
    
    private Currency curr;

}
