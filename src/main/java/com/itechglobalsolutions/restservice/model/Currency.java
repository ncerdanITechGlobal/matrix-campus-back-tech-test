package com.itechglobalsolutions.restservice.model;

public enum Currency {
  EURO,
  DOLLAR,
}
