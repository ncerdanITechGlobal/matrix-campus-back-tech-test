package com.itechglobalsolutions.restservice;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = RestServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PriceControllerIntegrationTests {

  @LocalServerPort
  private int port;
  
  TestRestTemplate restTemplate = new TestRestTemplate();

  HttpHeaders headers = new HttpHeaders();

  private void launchGetTest(String productId, String brandId, String timeStamp, String expectedResponse) throws JSONException {
    HttpEntity<String> entity = new HttpEntity<>(null, headers);

    ResponseEntity<String> response = restTemplate.exchange(
      String.format("http://localhost:8080/prices/%s/%s?date=%s",
        brandId,
        productId,
        timeStamp
      ),
      HttpMethod.GET, entity, String.class);
    
    JSONAssert.assertEquals(expectedResponse, response.getBody(), true);


    System.out.println("response"+response);
  }

  @Test
  void firstTest() throws JSONException{
    String expected = 
    """
      {"productId":35455,"brandId":1,"startDate":"2020-06-14T00:00:00.000+00:00","endDate":"2020-12-31T23:59:59.000+00:00","price":35.5,"priceList":1}
    """;
    launchGetTest("35455", "1", "1592128800000", expected);
  }
  
  @Test
  void secondTest() throws JSONException{
    String expected = 
    """
      {"productId":35455,"brandId":1,"startDate":"2020-06-14T15:00:00.000+00:00","endDate":"2020-06-14T18:30:00.000+00:00","price":25.45,"priceList":2}
    """;
    launchGetTest("35455", "1", "1592150400000", expected);
  }
  
  @Test
  void thirdTest() throws JSONException{
    String expected = 
      """
        {"productId":35455,"brandId":1,"startDate":"2020-06-14T00:00:00.000+00:00","endDate":"2020-12-31T23:59:59.000+00:00","price":35.5,"priceList":1}
      """;
    launchGetTest("35455", "1", "1592168400000", expected);
  }

  @Test
  void fourthTest() throws JSONException{
    String expected = 
      """
        {"productId":35455,"brandId":1,"startDate":"2020-06-15T00:00:00.000+00:00","endDate":"2020-06-15T11:00:00.000+00:00","price":30.5,"priceList":3}
      """;
    launchGetTest("35455", "1", "1592215200000", expected);
  }

  @Test
  void fifthTest() throws JSONException{
    String expected = 
      """
        {"productId":35455,"brandId":1,"startDate":"2020-06-15T16:00:00.000+00:00","endDate":"2020-12-31T23:59:59.000+00:00","price":38.95,"priceList":4}
      """;
    launchGetTest("35455", "1", "1592341200000", expected);
  }
  
}
